<?php echo $this->Html->docType('xhtml-strict'); ?>
<html lang="pt-BR">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');
		
		echo $this->fetch('meta');
		
		echo $this->Html->css('1140');
		echo $this->Html->css('ie');
		echo $this->Html->css('principal');
		echo $this->fetch('css');
		
		echo $this->Html->script('jquery-1.9.1.min');
		echo $this->Html->script('respond.min');
		echo $this->fetch('script');
	?>
</head>
<body>
	<!-- Barra do Governo -->
	<?php echo $this->element('layout/barra_governo'); ?>
	
	<!-- Cabeçalho -->
	<div id="header" class="container">
		<div class="row">
			<div class="fivecol">
				<h1><a href="/"><img src="/img/logo.png" alt="Radioweb IFRS" title="Página Inicial" /></a></h1>
			</div>
			<div class="fourcol"></div>
			<div class="threecol last">
				<img id="logo_ifrs" src="/img/logo_ifrs.png" alt="Logotipo do IFRS"/>
			</div>
		</div>
	</div>
	
	<!-- Flash Message -->
	<?php if ($this->Session->check('Message.flash')): ?>
	<div id="flash" class="container">
		<div class="row">
			<div class="onecol"></div>
			<div class="tencol">
				<?php echo $this->Session->flash(); ?>
			</div>
			<div class="onecol last"></div>
		</div>
	</div>
	<?php endif; ?>
		
	<!-- Conteúdo -->
	<div id="content" class="container">
		<a id="inicioConteudo" href="#inicioConteudo" class="oculto" accesskey="2">In&iacute;cio do conte&uacute;do</a>
		<?php echo $this->fetch('content'); ?>
		<a href="#" class="oculto">Fim do conte&uacute;do</a>
	</div>
	
	<!-- Rodapé -->
	<div id="footer" class="container">
		<div class="row">
			<div class="twelvecol">
				<div id="voltarTopo">
					<a href="#barra_governo">Topo da p&aacute;gina</a>
				</div>
				<p class="titulo">Instituto Federal de Educa&ccedil;&atilde;o, Ci&ecirc;ncia e Tecnologia do Rio Grande do Sul - IFRS</p>
				<address>Rua General Os&oacute;rio, 348 | Bairro Centro | CEP: 95700-000 | Bento Gon&ccedil;alves/RS</address>
				<p class="copyright"><a href="https://bitbucket.org/ricardomoro/radioweb/">Código-fonte deste sítio sob a licença GPL v3</a></p>
			</div>
		</div>
	</div>
	<?php echo $this->element('sql_dump'); ?>
</body>
</html>
