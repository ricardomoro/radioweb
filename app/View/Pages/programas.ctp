<div class="row">
	<div class="eightcol">
		<div id="div_principal">
			<h2><img src="/img/programas.png" alt="Programas" title="Programas" /></h2>
			<p>
				Todos os programas abaixo s&atilde;o de dom&iacute;nio p&uacute;blico, desde que citada a autoria e reproduzidos na &iacute;ntegra.<br/>
				S&atilde;o &aacute;udios com dura&ccedil;&atilde;o de um a dois minutos produzidos por estudantes do IFRS.<br/>
				O conte&uacute;do n&atilde;o &eacute; publicidade ou propaganda do instituto, mas comp&otilde;e-se de informa&ccedil;&otilde;es &uacute;teis para a sociedade.
			</p>
			<br/>
			<h2>Programa Educa&ccedil;&atilde;o no Ar</h2>
			<h3>C&acirc;mpus Bento Gonçalves</h3>
			<ul>
			<?php
				foreach (scandir(WWW_ROOT . 'files' . DS . 'programas' . DS . 'bento' . DS) as $file):
					if ($file == '.' || $file == '..') continue;
			?>
					<li><a target="_blank" href="/files/programas/bento/<?php echo $file; ?>"><?php echo preg_replace('/\.[^.]+$/', '', $file); ?></a></li>
			<?php
				endforeach;
			?>
			</ul>
			<h3>C&acirc;mpus Caxias do Sul</h3>
			<ul>
			<?php
				foreach (scandir(WWW_ROOT . 'files' . DS . 'programas' . DS . 'caxias' . DS) as $file):
					if ($file == '.' || $file == '..') continue;
			?>
					<li><a target="_blank" href="/files/programas/caxias/<?php echo $file; ?>"><?php echo preg_replace('/\.[^.]+$/', '', $file); ?></a></li>
			<?php
				endforeach;
			?>
			</ul>
			<h3>C&acirc;mpus Farroupilha</h3>
			<ul>
			<?php
				foreach (scandir(WWW_ROOT . 'files' . DS . 'programas' . DS . 'farroupilha' . DS) as $file):
					if ($file == '.' || $file == '..') continue;
			?>
					<li><a target="_blank" href="/files/programas/farroupilha/<?php echo $file; ?>"><?php echo preg_replace('/\.[^.]+$/', '', $file); ?></a></li>
			<?php
				endforeach;
			?>
			</ul>

			<hr/>

			<h2>Programa Curiosidades do Cinema</h2>
			<h3>Reitoria</h3>
			<ul>
			<?php
				foreach (scandir(WWW_ROOT . 'files' . DS . 'programas' . DS . 'reitoria' . DS) as $file):
					if ($file == '.' || $file == '..') continue;
			?>
					<li><a target="_blank" href="/files/programas/reitoria/<?php echo $file; ?>"><?php echo preg_replace('/\.[^.]+$/', '', $file); ?></a></li>
			<?php
				endforeach;
			?>
			</ul>
		</div>
	</div>
	<div class="fourcol last">
		<img src="/img/headphone2.png" />
	</div>
</div>