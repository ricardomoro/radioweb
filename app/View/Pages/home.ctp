<div class="row">
	<div class="fivecol">
		<div id="div_principal">
			<h2><img src="/img/o_projeto.png" alt="O Projeto" title="O Projeto" /></h2>
			<p>
				Em desenvolvimento desde 2012, o projeto Radioweb IFRS objetiva divulgar as a&ccedil;&otilde;es do instituto com conte&uacute;do gerado por todos os c&acirc;mpus.
			</p>
			<br/>
			<p>
				Os programas devem ser publicados em uma r&aacute;dio via internet e disponibilizados tamb&eacute;m por aplicativo para <em>smartphones</em>.
			</p>
			<br/>
			<p>
				O conte&uacute;do tamb&eacute;m fica dispon&iacute;vel para outras r&aacute;dios, mesmo as comerciais.
				Elas podem baixar os programas e veicular como quiserem, desde que respeitem o direito autoral e veiculem os programas na &iacute;ntegra.
			</p>
			<br/>
			<p>
				O projeto prev&ecirc; a instala&ccedil;&atilde;o de uma central na Reitoria, em Bento Gon&ccedil;alves, e laborat&oacute;rios de produ&ccedil;&atilde;o e difus&atilde;o radiof&ocirc;nica em cada c&acirc;mpus.
				A previs&atilde;o de estrutura&ccedil;&atilde;o completa &eacute; para 2014.
				Enquanto a estrutura &eacute; montada e os programas s&atilde;o projetados, os c&acirc;mpus participam do programa <strong>Educa&ccedil;&atilde;o no Ar</strong>.
			</p>
		</div>
	</div>
	<div id="headphone" class="fourcol">
		<img src="/img/headphone2.png" alt="Esta é uma página provisória desenvolvida apenas para divulgar o projeto entre a comunidade acadêmica do IFRS." />
	</div>
	<div id="coluna_direita" class="threecol last">
		<h2><img src="/img/programa_educacao_no_ar.png" alt="Programa Educação no Ar" title="Programa Educação no Ar" /></h2>
		<p>
			Para facilitar a produ&ccedil;&atilde;o, grava&ccedil;&atilde;o e edi&ccedil;&atilde;o, foram pensados micro programas com dura&ccedil;&atilde;o entre um e dois minutos.
			O formato, curto e com informa&ccedil;&otilde;es de interesse social, tamb&eacute;m se habilita para uso f&aacute;cil em outras r&aacute;dios, inclusive comerciais.
		</p>
		<p>
			Desde novembro de 2012, os c&acirc;mpus Bento Gon&ccedil;alves, Caxias do Sul e Farroupilha passaram a produzir esses programas.
			Eles s&atilde;o veiculados em v&aacute;rios hor&aacute;rios e diariamente em r&aacute;dios da Serra Ga&uacute;cha.
		</p>
		<p>
			O projeto &eacute; aberto a todos os c&acirc;mpus.
		</p>
		<br/>
		<h2><img src="/img/participe_voce_tambem.png" alt="Participe você também" title="Participe você também" /></h2>
		<p>
			Voc&ecirc;, estudante ou servidor do IFRS tamb&eacute;m pode participar.
			Procure o comunicador do seu c&acirc;mpus e informe-se.
		</p>
		<a href="/pages/programas" title="Ouça os programas"><img src="/img/ouca_os_programas.png" alt="Ouça os programas" /></a>
	</div>
</div>